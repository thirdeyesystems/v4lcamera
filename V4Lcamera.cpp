#include <cassert>
#include <chrono>
#include <fcntl.h>
#include <iostream>
#include <linux/ioctl.h>
#include <linux/types.h>
#include <linux/v4l2-common.h>
#include <linux/v4l2-controls.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <thread>
#include <time.h>
#include <unistd.h>

#include "V4Lcamera.h"
#include "libcsi_ioctl.h"
#include "utils.h"

using namespace std;
using namespace std::chrono;

long long timeInMilliseconds(void) {
  struct timeval tv;

  gettimeofday(&tv, NULL);
  return (((long long)tv.tv_sec) * 1000) + (tv.tv_usec / 1000);
}

V4L_Camera::V4L_Camera(const V4L_Camera &) { assert(false); }

V4L_Camera::V4L_Camera() { mChangeExposureDone = true; }

V4L_Camera::~V4L_Camera() { DeInit(); }

int V4L_Camera::setblocking(bool blocking) {
  int flags = fcntl(fd, F_GETFL);
  if (flags < 0) {
    printf("failed to get flag status\r\n");
    return 1;
  }

  if (blocking) {
    if ((flags & O_NONBLOCK) != 0) // check if bit is not set
    {
      flags &= ~O_NONBLOCK;
      if (fcntl(fd, F_SETFL, flags) < 0) {
        printf("failed to clear O_NONBLOCK flag\r\n");
        return 1;
      }

      flags = fcntl(fd, F_GETFL);
      if ((flags & O_NONBLOCK) != 0) // check if bit is set
      {
        printf("should not be here\r\n");
      }
    }
  } else {
    if ((flags & O_NONBLOCK) == 0) // check if bit is not set
    {
      flags |= O_NONBLOCK;
      if (fcntl(fd, F_SETFL, flags) < 0) {
        printf("failed to set O_NONBLOCK flag\r\n");
        return 1;
      }

      flags = fcntl(fd, F_GETFL);
      if ((flags & ~O_NONBLOCK) == 0) // check if bit is not set
      {
        printf("should not be here\r\n");
      }
    }
  }
  return 0;
}

int V4L_Camera::Init(const std::string &path, unsigned int width,
                     unsigned int height, unsigned int mOffsetX,
                     unsigned int mOffsetY, unsigned int pixelFormat,
                     TRIGGER_MODE trigger_mode) {
  this->path = path;
  mTriggerMode = trigger_mode;
  // Open the device
  fd = open(path.c_str(), O_RDWR); // A file descriptor to the video device
  if (fd < 0) {
    fprintf(stderr, "ERROR: Failed to open device, OPEN\r\n");
    return 1;
  }

  // Ask the device if it can capture frames
  v4l2_capability capability;
  if (ioctl(fd, VIDIOC_QUERYCAP, &capability) < 0) {
    // something went wrong... exit
    fprintf(stderr,
            "ERROR: Failed to get device capabilities, VIDIOC_QUERYCAP\r\n");
    return 1;
  }
  if (!(capability.capabilities & V4L2_CAP_STREAMING)) {
    fprintf(stderr, "%s does not support streaming i/o\r\n", path.c_str());
    exit(EXIT_FAILURE);
  }

  cameraName = std::string((char *)capability.card);

  {
    // Get Image format
    imageFormat.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    // Get Image format
    if (ioctl(fd, VIDIOC_G_FMT, &imageFormat) < 0) {
      fprintf(stderr, "ERROR: Device could not get format, VIDIOC_G_FMT\r\n");
      return 1;
    }

    // if (width > 0)
    // {
    //     if (imageFormat.fmt.pix.width != width)
    //     {
    //         cout << "changing width from (" <<
    //         to_string(imageFormat.fmt.pix.width) << ") to (" <<
    //         to_string(width) << ")" << endl; imageFormat.fmt.pix.width =
    //         width;
    //     }
    // }

    // if (height > 0)
    // {
    //     if (imageFormat.fmt.pix.height != height)
    //     {
    //         cout << "changing height from (" <<
    //         to_string(imageFormat.fmt.pix.height) << ") to (" <<
    //         to_string(height) << ")" << endl; imageFormat.fmt.pix.height =
    //         height;
    //     }
    // }
    imageFormat.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (pixelFormat < 0)
      cout << "pixel format not supplied" << endl;

    else if (imageFormat.fmt.pix.pixelformat != pixelFormat) {
      cout << "changing pixel format from ("
           << FourCCToString(imageFormat.fmt.pix.pixelformat) << ") to ("
           << FourCCToString(pixelFormat) << ")" << endl;
      imageFormat.fmt.pix.pixelformat = pixelFormat;
    }

    imageFormat.fmt.pix.field = V4L2_FIELD_ANY;
    // tell the device you are using this format
    if (ioctl(fd, VIDIOC_S_FMT, &imageFormat) < 0) {
      fprintf(stderr, "ERROR: Device could not set format, VIDIOC_S_FMT\r\n");
      cout << strerror(errno) << endl;
      return 1;
    }
  }
  // CROP
  {
    if (ioctl(fd, VIDIOC_G_CROP, &imageCrop) < 0) {
      fprintf(stderr, "ERROR: Device could not get crop, VIDIOC_G_CROP\r\n");
      cout << strerror(errno) << endl;
      return 1;
    }

    if (imageCrop.c.height != height) {
      std::cout << "changing height from (" << to_string(imageCrop.c.height)
                << ") to (" << to_string(height) << ")" << std::endl;
      imageCrop.c.height = height;
    }

    if (imageCrop.c.top != mOffsetY) {
      std::cout << "changing offset Y from (" << to_string(imageCrop.c.top)
                << ") to (" << to_string(mOffsetY) << ")" << std::endl;
      imageCrop.c.top = mOffsetY;
    }

    if (imageCrop.c.left != mOffsetX) {
      std::cout << "changing offset X from (" << to_string(imageCrop.c.left)
                << ") to (" << to_string(mOffsetX) << ")" << std::endl;
      imageCrop.c.left = mOffsetX;
    }

    if (imageCrop.c.width != width) {
      std::cout << "changing width from (" << to_string(imageCrop.c.width)
                << ") to (" << to_string(width) << ")" << std::endl;
      imageCrop.c.width = width;
    }

    if (ioctl(fd, VIDIOC_S_CROP, &imageCrop) < 0) {
      fprintf(stderr, "ERROR: Device could not set crop, VIDIOC_S_CROP\r\n");
      cout << strerror(errno) << endl;
      return 1;
    }

    if (ioctl(fd, VIDIOC_G_CROP, &imageCrop) < 0) {
      fprintf(stderr, "ERROR: Device could not get crop, VIDIOC_G_CROP\r\n");
      cout << strerror(errno) << endl;
      return 1;
    } else {
      bool failed = false;
      if (imageCrop.c.width != width) {
        printf("imageCrop.c.width != width %u != %u\r\n", imageCrop.c.width,
               width);
        failed = true;
      }

      if (imageCrop.c.height != height) {
        printf("imageCrop.c.height != height %u != %u\r\n", imageCrop.c.height,
               height);
        failed = true;
      }

      if (imageCrop.c.top != mOffsetY) {
        printf("imageCrop.c.top != mOffsetY %u != %u\r\n", imageCrop.c.top,
               mOffsetY);
        failed = true;
      }

      if (imageCrop.c.left != mOffsetX) {
        printf("imageCrop.c.left != mOffsetX %u != %u\r\n", imageCrop.c.left,
               mOffsetX);
        failed = true;
      }

      if (failed)
        return 1;
    }
  }

  // Request Buffers from the device
  v4l2_requestbuffers requestBuffer = {0};
  requestBuffer.count = nBuffers; // one request buffer
  requestBuffer.type =
      V4L2_BUF_TYPE_VIDEO_CAPTURE; // request a buffer wich we an use for
                                   // capturing frames
  requestBuffer.memory = V4L2_MEMORY_MMAP;

  if (ioctl(fd, VIDIOC_REQBUFS, &requestBuffer) < 0) {
    fprintf(stderr,
            "ERROR: Could not request buffer from device, VIDIOC_REQBUFS\r\n");
    return 1;
  }

  for (unsigned bufIdx = 0; bufIdx < requestBuffer.count; ++bufIdx) {
    // Quety the buffer to get raw data ie. ask for the you requested buffer
    // and allocate memory for it
    v4l2_buffer &bufferinfo = mBufferInfo[bufIdx];
    bufferinfo.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    bufferinfo.memory = V4L2_MEMORY_MMAP;
    bufferinfo.index = bufIdx;
    if (ioctl(fd, VIDIOC_QUERYBUF, &bufferinfo) < 0) {
      fprintf(stderr, "ERROR: Device did not return the buffer information, "
                      "VIDIOC_QUERYBUF\r\n");
      return 1;
    }
    // use a pointer to point to the newly created buffer
    // mmap() will map the memory address of the device to
    // an address in memory
    buffer[bufIdx] =
        (unsigned char *)mmap(NULL, bufferinfo.length, PROT_READ | PROT_WRITE,
                              MAP_SHARED, fd, bufferinfo.m.offset);
    if (buffer[bufIdx] == MAP_FAILED) {
      printf("mmap failed\r\n");
      return 1;
    }
    memset(buffer[bufIdx], 0, bufferinfo.length);
  }

  for (unsigned bufIdx = 0; bufIdx < requestBuffer.count; ++bufIdx) {
    v4l2_buffer &bufferinfo = mBufferInfo[bufIdx];
    // Queue the buffer
    if (ioctl(fd, VIDIOC_QBUF, &bufferinfo) < 0) {
      fprintf(stderr, "ERROR: Could not queue buffer, VIDIOC_QBUF\r\n");
      return 1;
    }
  }

  if (mTriggerMode == TRIGGER_MODE::MASTER) {
    if (ioctl(fd, VIDIOC_TRIGGER_MODE_OFF) < 0) {
      printf("enabling trigger mode failed\r\n");
      return 1;
    }
  } else {
    // Set trigger mode
    if (ioctl(fd, VIDIOC_TRIGGER_MODE_ON) < 0) {
      printf("enabling trigger mode failed\r\n");
      return 1;
    }

    // Set trigger source to software
    if (mTriggerMode == TRIGGER_MODE::SOFTWARE) {
      int const source = V4L2_TRIGGER_SOURCE_SOFTWARE;
      if (ioctl(fd, VIDIOC_S_TRIGGER_SOURCE, &source) < 0) {
        printf("setting trigger source to software failed\r\n");
        return 1;
      }
    } else if (mTriggerMode == TRIGGER_MODE::HARDWARE) {
      // Set trigger source to Hardware
      int const source = V4L2_TRIGGER_SOURCE_LINE0;
      if (ioctl(fd, VIDIOC_S_TRIGGER_SOURCE, &source) < 0) {
        printf("setting trigger source to hardware failed\r\n");
        return 1;
      }
    } else {
      assert(true);
    }

    // Set trigger activation
    int const activation = V4L2_TRIGGER_ACTIVATION_RISING_EDGE;
    if (ioctl(fd, VIDIOC_S_TRIGGER_ACTIVATION, &activation) < 0) {
      printf("setting activation failed\r\n");
      return 1;
    }
  }
  // setblocking(false);

  mExposureType = GetAutoExposure();
  // if (mExposureType == V4L2_EXPOSURE_AUTO)
  //     std::cout << "exposure = V4L2_EXPOSURE_AUTO" << std::endl;
  // else if (mExposureType == V4L2_EXPOSURE_MANUAL)
  //     std::cout << "exposure = V4L2_EXPOSURE_MANUAL" << std::endl;

  return 0;
}

int V4L_Camera::StartStream() {
  // Activate streaming
  int type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if (ioctl(fd, VIDIOC_STREAMON, &type) < 0) {
    fprintf(stderr, "ERROR: Could not start streaming, VIDIOC_STREAMON\r\n");
    return 1;
  }

  if (!TRIGGER_MODE::MASTER) {
    // printf("Pre-triggering frames (vi driver limitation workaround)\r\n");
    // for (int i = 0; i < nBuffers - 1; i++)
    // {
    //     printf("start trigger\r\n");
    //     if (-1 == ioctl(fd, VIDIOC_TRIGGER_SOFTWARE))
    //     {
    //         printf("VIDIOC_TRIGGER_SOFTWARE failed\r\n");
    //         return 1;
    //     }
    //     usleep(2000000);
    // }
  }

  streaming = 1;
  return 0;
}

int V4L_Camera::StopStream() {
  // end streaming
  int type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if (ioctl(fd, VIDIOC_STREAMOFF, &type) < 0) {
    fprintf(stderr, "ERROR: Could not end streaming, VIDIOC_STREAMOFF\r\n");
    return 1;
  }
  streaming = 0;

  return 0;
}

bool V4L_Camera::GetChangeExposureDone() { return mChangeExposureDone; }

bool V4L_Camera::DoSnap() {
  steady_clock::time_point start = steady_clock::now();
  while (!mChangeExposureDone) {
    // std::cout << "NOOO exposure thread didn't finished yet :(" << std::endl;
  }

  const int64_t dur =
      duration_cast<milliseconds>(steady_clock::now() - start).count();
  if (dur > 0)
    printf("NOOO exposure thread didn't finished yet :( - %ldms\r\n", dur);

  std::lock_guard<std::mutex> guard(mIOCTL_mtx);

  if (mTriggerMode == TRIGGER_MODE::SOFTWARE) {
    int rc;

    // printf("ts: %lu,\tVIDIOC_TRIGGER_SOFTWARE...\r\n", ts);
    // setblocking(false);

    steady_clock::time_point ts = steady_clock::now();

    rc = ioctl(fd, VIDIOC_TRIGGER_SOFTWARE);
    // rc = ioctl(fd, VIDIOC_TRIGGER_SOFTWARE);

    // const int64_t dt = duration_cast<milliseconds>(
    //                        steady_clock::now() - ts)
    //                        .count();
    // printf("dt: %ldms,\tVIDIOC_TRIGGER_SOFTWARE, rc = %d\r\n", dt, rc);
    // if (dt > 11)
    // {
    //     printf("tring again DoSnap()\r\n");
    //     DoSnap();
    // }

    if (rc) {
      printf("failed VIDIOC_TRIGGER_SOFTWARE, error: %s\r\n", strerror(rc));
      return false;
    }

    return true;
  }
  return true;
}

const unsigned char *V4L_Camera::GetFrame(FrameProps *frame_props) {
  int rc;
  assert(frame_props);

  v4l2_buffer &bufferinfo = mBufferInfo[mBufIdx];
  // setblocking(true);
  assert(bufferinfo.type == V4L2_BUF_TYPE_VIDEO_CAPTURE);
  assert(bufferinfo.memory == V4L2_MEMORY_MMAP);

  // std::cout << "Dequeue_image..." << std::endl;
  rc = Dequeue_image(bufferinfo);
  // std::cout << "Dequeue_image... -Done" << std::endl;
  mBufIdx = (mBufIdx + 1) % nBuffers;
  if (rc) {
    printf("failed to Dequeue_image\r\n");
    return nullptr;
  }

  if (bufferinfo.sequence == frame_props->frame_idx) {
    Queue_image(bufferinfo);
    printf("failed to bufferinfo. sequence wasn't changed\r\n");
    return nullptr;
  }

  frame_props->frame_idx = bufferinfo.sequence;
  frame_props->timestamp_us =
      bufferinfo.timestamp.tv_sec * 1000000 + bufferinfo.timestamp.tv_usec;

  // cout << std::to_string(frame_props->frame_idx) << ":\t";
  // cout << "timestamp: " << std::to_string(1.0f * frame_props->timestamp_us /
  // 1000000) << "\t"; cout << "Buffer has: " << (double)bufferinfo.bytesused /
  // 1024 << " KBytes of data" << endl;

  Queue_image(bufferinfo);
  return buffer[bufferinfo.index];
}

int V4L_Camera::DeInit() {
  if (fd >= 0) {
    close(fd);
    fd = -1;
  }
  return 0;
}

int V4L_Camera::Dequeue_image(v4l2_buffer &bufferinfo) {
  int rc;

  // Dequeue the buffer
  // printf("VIDIOC_DQBUF...\r\n");
  steady_clock::time_point ts = steady_clock::now();

  std::lock_guard<std::mutex> guard(mIOCTL_mtx);
  do {
    struct pollfd pfds[1];
    pfds[0].fd = fd;
    pfds[0].events = POLLIN;
    rc = poll(pfds, 1, 150);
    if (rc < 0 || !(pfds[0].revents & POLLIN)) {
      printf("failed to get buffer from %s, rc = %d\r\n", cameraName.c_str(),
             rc);
      if (rc == -1)
        printf("errno : %d - %s\r\n", errno, strerror(errno));
      return 1;
    }

    rc = ioctl(fd, VIDIOC_DQBUF, &bufferinfo);
    // printf("VIDIOC_DQBUF, rc = %d\r\n", rc);
    if (rc) {
      if (errno == EACCES || errno == EAGAIN) {
        printf("Already locked by another process (VIDIOC_DQBUF)\r\n");
        return Dequeue_image(bufferinfo);
      }
      const int64_t dt =
          duration_cast<milliseconds>(steady_clock::now() - ts).count();
      if (dt > 100) {
        printf("Could not dequeue the buffer, VIDIOC_DQBUF, error: %s\r\n",
               strerror(errno));
        return 1;
      }
      usleep(1000);
    } else {
      return 0;
    }
  } while (errno == EACCES || errno == EAGAIN);

  // Frames get written after dequeuing the buffer
  return 0;
}

int V4L_Camera::Queue_image(v4l2_buffer &bufferinfo) {
  int rc = 1;
  // Queue the buffer
  // printf("VIDIOC_QBUF...\r\n");
  rc = ioctl(fd, VIDIOC_QBUF, &bufferinfo);
  // printf("VIDIOC_QBUF, rc = %d\r\n", rc);
  if (rc) {
    printf("Could not queue buffer, VIDIOC_QBUF, error: %s\r\n", strerror(rc));
    return 1;
  }
  return 0;
}

std::string V4L_Camera::GetImageFormat() const {
  return FourCCToString(imageFormat.fmt.pix.pixelformat);
}

unsigned int V4L_Camera::GetimageSize() const {
  return mBufferInfo[0].bytesused;
}

static std::string autoExposureType(int type) {
  switch (type) {
  case V4L2_EXPOSURE_AUTO:
    return std::string("V4L2_EXPOSURE_AUTO");
  case V4L2_EXPOSURE_MANUAL:
    return std::string("V4L2_EXPOSURE_MANUAL");
  case V4L2_EXPOSURE_SHUTTER_PRIORITY:
    return std::string("V4L2_EXPOSURE_SHUTTER_PRIORITY");
  case V4L2_EXPOSURE_APERTURE_PRIORITY:
    return std::string("V4L2_EXPOSURE_APERTURE_PRIORITY");
  default:
    fprintf(stderr, "ERROR: unknown exposure type");
    return "";
  }
}

int V4L_Camera::SetAutoGain(bool setAuto) {
  struct v4l2_control ctrl = {0};
  ctrl.id = V4L2_CID_AUTOGAIN;
  if (setAuto)
    ctrl.value = 1;
  else
    ctrl.value = 0;
  printf("SetAutoGain to %s\r\n", std::to_string(setAuto).c_str());
  if (-1 == ioctl(fd, VIDIOC_S_CTRL, &ctrl)) {
    fprintf(stderr, "ERROR: setting V4L2_CID_AUTOGAIN\r\n");
    return -1;
  }
  mGainType = setAuto;
  return 0;
}

/*
V4L2_EXPOSURE_MANUAL and V4L2_EXPOSURE_APERTURE_PRIORITY are commonly used.
*/
int V4L_Camera::SetAutoExposure(v4l2_exposure_auto_type type) {
  struct v4l2_control ctrl = {0};
  ctrl.id = V4L2_CID_EXPOSURE_AUTO;
  ctrl.value = type;
  printf("SetAutoExposure to %s\r\n", autoExposureType(type).c_str());
  if (-1 == ioctl(fd, VIDIOC_S_CTRL, &ctrl)) {
    fprintf(stderr, "ERROR: setting V4L2_CID_EXPOSURE_AUTO\r\n");
    return -1;
  }
  mExposureType = type;
  return type;
}

int V4L_Camera::GetAutoExposure() const {
  struct v4l2_control ctrl = {0};
  ctrl.id = V4L2_CID_EXPOSURE_AUTO;
  if (-1 == ioctl(fd, VIDIOC_G_CTRL, &ctrl)) {
    fprintf(stderr, "ERROR: getting V4L2_CID_EXPOSURE_AUTO\r\n");
    return -1;
  }
  // printf("GetAutoExposure=%s\r\n", autoExposureType(ctrl.value).c_str());
  return ctrl.value;
}

int V4L_Camera::GetAutoGain() const {
  struct v4l2_control ctrl = {0};
  ctrl.id = V4L2_CID_AUTOGAIN;
  if (-1 == ioctl(fd, VIDIOC_G_CTRL, &ctrl)) {
    fprintf(stderr, "ERROR: getting V4L2_CID_AUTOGAIN\r\n");
    return -1;
  }
  // printf("GetAutoExposure=%s\r\n", autoExposureType(ctrl.value).c_str());
  return ctrl.value;
}

void V4L_Camera::getExposureBounds(int &eMin, int &eMax) {
  struct v4l2_queryctrl queryctrl;
  memset(&queryctrl, 0, sizeof(queryctrl));
  queryctrl.id = V4L2_CID_EXPOSURE_ABSOLUTE;

  if (-1 == ioctl(fd, VIDIOC_QUERYCTRL, &queryctrl)) {
    if (errno != EINVAL) {
      fprintf(stderr, "ERROR: VIDIOC_QUERYCTRL\r\n");
      exit(EXIT_FAILURE);
    } else {
      printf(">> V4L2_CID_EXPOSURE_ABSOLUTE is not supported\r\n");
      eMin = -1;
      eMax = -1;
    }
  } else if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED) {
    printf(">> V4L2_CID_EXPOSURE_ABSOLUTE is not supported\r\n");
    eMin = -1;
    eMax = -1;
  } else {
    /*cout << "Name    : " << queryctrl.name << endl;
    cout << "Min     : " << queryctrl.minimum << endl;
    cout << "Max     : " << queryctrl.maximum << endl;
    cout << "Step    : " << queryctrl.step << endl;
    cout << "Default : " << queryctrl.default_value << endl;
    cout << "Flags   : " << queryctrl.flags << endl;*/

    eMin = queryctrl.minimum;
    eMax = queryctrl.maximum;
  }
}

/*
    The exposure time is limited by the frame interval. Drivers should interpret
    the values as 100 µs units, where the value 1 stands for 1/10000th of a
   second, 10000 for 1 second and 100000 for 10 seconds. if 30fps is the goal,
   each frame takes 1/30s = 33ms = 33*10*100us = 330 * 100us. It depends on the
   ambient light to tuning exposure time.
*/
int V4L_Camera::SetManualExposure(int val) {
  std::lock_guard<std::mutex> guard(mIOCTL_mtx);
  struct v4l2_control ctrl = {0};
  if (mExposureType != V4L2_EXPOSURE_MANUAL) {
    if (SetAutoExposure(V4L2_EXPOSURE_MANUAL) != V4L2_EXPOSURE_MANUAL) {
      fprintf(stderr, "ERROR: setting V4L2_EXPOSURE_MANUAL\r\n");
      mChangeExposureDone = true;
      return -1;
    }
  }

  // int min = -1, max = -1;
  // getExposureBounds(min, max);
  // if (val <= min || max <= val)
  // {
  //     fprintf(stderr, "ERROR: exposure %d[units of 100us] is out of bound
  //     (%d, %d)\r\n", val, min, max); return -1;
  // }

  // get exposure
  memset(&ctrl, 0, sizeof(ctrl));
  ctrl.id = V4L2_CID_EXPOSURE;
  if (-1 == ioctl(fd, VIDIOC_G_CTRL, &ctrl))
    fprintf(stderr, "ERROR: getting V4L2_CID_EXPOSURE failed\r\n");

  ctrl.value = val;
  long long startTime = timeInMilliseconds(); // get exposure
  if (-1 == ioctl(fd, VIDIOC_S_CTRL, &ctrl)) {
    fprintf(stderr, "ERROR: setting V4L2_CID_EXPOSURE failed\r\n");
    mChangeExposureDone = true;
    return -1;
  }

  mExposureTime = val;

  // long long exposurechangeTime = timeInMilliseconds() - startTime;
  // printf("\t\t\t\tExposure: %d\r\n", mExposureTime);

  mChangeExposureDone = true;
  return val;
}

int V4L_Camera::SetManualGain(int val) {
  std::lock_guard<std::mutex> guard(mIOCTL_mtx);
  struct v4l2_control ctrl = {0};
  if (mGainType != 0) {
    if (SetAutoGain(0) != 0) {
      fprintf(stderr, "ERROR: setting SetAutoGain\r\n");
      mChangeGainDone = true;
      return -1;
    }
  }

  // get gain
  memset(&ctrl, 0, sizeof(ctrl));
  ctrl.id = V4L2_CID_GAIN;
  if (-1 == ioctl(fd, VIDIOC_G_CTRL, &ctrl))
    fprintf(stderr, "ERROR: getting V4L2_CID_GAIN failed\r\n");

  ctrl.value = val;
  long long startTime = timeInMilliseconds(); // get gain
  if (-1 == ioctl(fd, VIDIOC_S_CTRL, &ctrl)) {
    fprintf(stderr, "ERROR: setting V4L2_CID_GAIN failed\r\n");
    mChangeGainDone = true;
    return -1;
  }

  mGain = val;

  // long long gainChangeTime = timeInMilliseconds() - startTime;
  // printf("\t\t\t\tExposure: %d\r\n", gainChangeTime);

  mChangeGainDone = true;
  return val;
}

int V4L_Camera::GetManualGain() const {
  struct v4l2_control ctrl = {0};
  ctrl.id = V4L2_CID_GAIN;
  if (-1 == ioctl(fd, VIDIOC_G_CTRL, &ctrl)) {
    fprintf(stderr, "ERROR: getting V4L2_CID_GAIN\r\n");
    return -1;
  }
  printf("GetManualGain=%d\r\n", ctrl.value);
  return ctrl.value;
}

int V4L_Camera::GetManualExposure() const {
  struct v4l2_control ctrl = {0};
  /*	if(GetAutoExposure(fd) != V4L2_EXPOSURE_MANUAL){
          fprintf(stderr, "ERROR: SetManualExposure is not in
     V4L2_EXPOSURE_MANUAL"); return -1;
      }
  */
  ctrl.id = V4L2_CID_EXPOSURE_ABSOLUTE;
  if (-1 == ioctl(fd, VIDIOC_G_CTRL, &ctrl)) {
    fprintf(stderr, "ERROR: getting V4L2_EXPOSURE_MANUAL\r\n");
    return -1;
  }
  printf("GetManualExposure=%d\r\n", ctrl.value);
  return ctrl.value;
}

int V4L_Camera::setExposure(uint32_t exposure, bool thread) {
  int ret = 0;
  //  Example 1: Get exposure mode, set to manual exposure mode

  // // Get exposure mode
  // ret = GetAutoExposure(fd);
  // if (ret < 0)
  // {
  //     printf("Get exposure auto Type failed\r\n");
  //     return -1; // V4L2_UTILS_GET_EXPSURE_AUTO_TYPE_ERR;
  // }

  if (exposure == 0) // auto exposure
  {
    ret = SetAutoExposure(v4l2_exposure_auto_type::V4L2_EXPOSURE_AUTO);
    if (ret < 0) {
      printf("Set AUTO exposure Type failed\r\n");
      return -1; // V4L2_UTILS_SET_EXPSURE_AUTO_TYPE_ERR;
    }
  } else if (exposure > 0) // manual exposure
  {
    if (mExposureTime == exposure) {
      // printf("Exposure is already: %d\r\n", exposure);
    } else {
      mChangeExposureDone = false;
      if (thread) {
        std::thread manualExposureThread(&V4L_Camera::SetManualExposure, this,
                                         exposure);
        manualExposureThread.detach();
      } else {
        int newExposure = SetManualExposure(exposure);
        if (newExposure <= 0)
          return -1;
      }

      // manualExposureThread.join();
      // ret = SetManualExposure(exposure);
      // if (ret < 0)
      // {
      //     printf("Set MANUAL exposure failed (% d)\r\n", ret);
      //     cout << strerror(errno) << endl;
      //     return -1; // V4L2_UTILS_SET_EXPSURE_ERR;
      // }
      // else
      // {
      //     // printf("exposure set to: %d.%dms\r\n", exposure / 10, exposure %
      //     10);
      // }
    }

    // printf("Get ABS Exposure Success: %d.%dms\r\n", getCtrl.value / 10,
    // getCtrl.value % 10);

    // sleep(1);

    // if (getCtrl.value != exposure)
    // {
    //     // Set the absolute value of exposure
    //     setCtrl.id = V4L2_CID_EXPOSURE_ABSOLUTE;
    //     setCtrl.value = exposure;
    //     ret = ioctl(fd, VIDIOC_S_CTRL, &setCtrl);
    //     if (ret < 0)
    //     {
    //         printf("Set exposure failed (%d)\r\n", ret);
    //         cout << strerror(errno) << endl;
    //         return -1; // V4L2_UTILS_SET_EXPSURE_ERR;
    //     }
    // }
  }

  /* // Example 2 : After setting to manual mode, get and set the exposure level
  int ret; ctrl.id = V4L2_CID_EXPOSURE; // Get exposure level, A20 accepts 9
  levels from -4 to 4 ret = ioctl(fd, VIDIOC_G_CTRL, &ctrl); if (ret < 0)
  {
      printf("Get exposure failed (% d)\r\n", ret);
      return -1; // V4L2_UTILS_GET_EXPSURE_ERR;
  }

  printf("\r\nGet Exposure: [% d]\r\n", ctrl.value);
  // Set the exposure level
  ctrl.id = V4L2_CID_EXPOSURE;
  ctrl.value = -4;
  ret = ioctl(fd, VIDIOC_S_CTRL, &ctrl);
  if (ret < 0)
  {
      printf("Set exposurefailed (% d)\r\n", ret);
      return -1; // V4L2_UTILS_SET_EXPSURE_ERR;
  }

  // Example 3 : After setting to manual mode, obtain and set the absolute value
  of exposure : int ret;

  ctrl.id = V4L2_CID_EXPOSURE_ABSOLUTE;
  ret = ioctl(fd, VIDIOC_G_CTRL, &ctrl);
  if (ret < 0)
  {
      printf("Set exposure failed (% d)\r\n", ret);
      // return V4L2_UTILS_SET_EXPSURE_ERR;
  }
  printf("\r\nGet ABS EXP Success: [% d]\r\n", ctrl.value);

  sleep(1);

  // Set the absolute value of exposure
  ctrl.id = V4L2_CID_EXPOSURE_ABSOLUTE;
  ctrl.value = 5;
  ret = ioctl(fd, VIDIOC_S_CTRL, &ctrl);
  if (ret < 0)
  {
      printf("Set exposure failed (% d)\r\n", ret);
      // return V4L2_UTILS_SET_EXPSURE_ERR;
  } */

  return 0;
}

int V4L_Camera::SetGain(uint32_t gain, bool thread) {
  int ret = 0;
  if (mGainType == 1) // auto gain
  {
    ret = SetAutoGain(0);
    if (ret < 0) {
      printf("Set AUTO gain Type failed\r\n");
      return -1;
    }
  }
  if (mGain == gain) {
    // printf("Gain is already: %d\r\n", exposure);
  } else {
    mChangeGainDone = false;
    if (thread) {
      std::thread manualGainThread(&V4L_Camera::SetManualGain, this, gain);
      manualGainThread.detach();
    } else {
      int newGain = SetManualGain(gain);
      if (newGain <= 0)
        return -1;
    }
  }
  return 0;
}

V4L_Camera::TRIGGER_MODE V4L_Camera::getTriggerMode() const {
  return mTriggerMode;
}

void V4L_Camera::WaitForThreads() const {
  while (!mChangeExposureDone || !mChangeGainDone) {
    if (!mChangeExposureDone)
      std::cout << "MIPI: exposure is not yet Ready" << std::endl;
    if (!mChangeGainDone)
      std::cout << "MIPI: gain is not yet Ready" << std::endl;
    usleep(1000);
  }
}

#define CLIP(color)                                                            \
  (unsigned char)(((color) > 0xFF) ? 0xff : (((color) < 0) ? 0 : (color)))
void v4lconvert_uyvy_to_rgb24(const unsigned char *src, unsigned char *dest,
                              int width, int height, int stride,
                              unsigned int pixfmt) {
  int j;

  while (--height >= 0) {
    for (j = 0; j + 1 < width; j += 2) {
      int u, v;

      if (pixfmt == V4L2_PIX_FMT_UYVY) {
        u = src[0];
        v = src[2];
      } else if (pixfmt == V4L2_PIX_FMT_VYUY) {
        u = src[2];
        v = src[0];
      } else
        assert(false && "Unsupported pixelFormat");

      int u1 = (((u - 128) << 7) + (u - 128)) >> 6;
      int rg = (((u - 128) << 1) + (u - 128) + ((v - 128) << 2) +
                ((v - 128) << 1)) >>
               3;
      int v1 = (((v - 128) << 1) + (v - 128)) >> 1;

      *dest++ = CLIP(src[1] + v1);
      *dest++ = CLIP(src[1] - rg);
      *dest++ = CLIP(src[1] + u1);
      *dest++ = CLIP(src[3] + v1);
      *dest++ = CLIP(src[3] - rg);
      *dest++ = CLIP(src[3] + u1);

      src += 4;
    }

    src += stride - width * 2;
  }
}
