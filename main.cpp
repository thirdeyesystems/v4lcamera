/*
    g++ -g ./main.cpp -o ./main
*/

#include <cstring>  //memcpy
#include <fstream>  //ofstream
#include <iostream> //cout
#include <string>
#include <unistd.h>

#include "../../Chimera/GPIO.h"
#include "V4Lcamera.h"
#include "utils.h"

using namespace std;
int main() {
  std::string cameraPath = "/dev/video0";
  std::string cameraType = "1620m";
  int rc;

  unsigned int width = 0;
  unsigned int height = 0;
  int pixelFormat;

  if (0 == cameraType.compare("2050c")) {
    width = 5376;
    height = 3672;
    pixelFormat = V4L2_PIX_FMT_SRGGB8;
  } else if (0 == cameraType.compare("1236c")) {
    width = 4112;
    height = 3008;
    pixelFormat = V4L2_PIX_FMT_SRGGB8;
  } else if (0 == cameraType.compare("1620m")) {
    width = 5312;
    height = 3040;
    pixelFormat = V4L2_PIX_FMT_GREY;
  } else if (0 == cameraType.compare("1242c")) {
    width = 2944;
    height = 2944;
    pixelFormat = V4L2_PIX_FMT_GREY;
  } else {
    exit(1);
  }

  for (int indexindex = 0; indexindex < 1; indexindex++) // why is it important?
  {
    std::ofstream outFile;
  }

  V4L_Camera camera = V4L_Camera();

  const V4L_Camera::TRIGGER_MODE triggerMode =
      V4L_Camera::TRIGGER_MODE::SOFTWARE;
  GPIO *triggerGPIO = nullptr;

  if (triggerMode == V4L_Camera::TRIGGER_MODE::HARDWARE) {
    const uint16_t gpioPin = 444;
    triggerGPIO = new GPIO(gpioPin, GPIO::GPIO_DIRECTION::GPIO_DIRECTION_OUT,
                           GPIO::GPIO_INTERRUPT_EDGE::GPIO_INTERRUPT_EDGE_NONE);
    triggerGPIO->init();
  }

  rc = camera.Init(cameraPath, width, height, 0, 0, pixelFormat, triggerMode);
  if (rc)
    exit(1);
  camera.StartStream();
  cout << camera.GetName() << endl;
  int isAutoGain = camera.GetAutoGain();

  int gain = -1;
  gain = camera.GetManualGain();
  int gainRC = 0;
  // gainRC = camera.SetAutoGain(0); // SetAutoGain = 0 for manual mode
  gainRC = camera.SetGain(gain - 1);
  gain = camera.GetManualGain();

  struct timespec start, end;
  clock_gettime(CLOCK_MONOTONIC_RAW, &start);

  std::string path;
  struct FrameProps frame_props;
  const int n_frames = 10;
  int exposure = 0;
  for (int frameIdx = 0; frameIdx < n_frames; frameIdx++) {
    exposure = 127000 + 1000 * frameIdx;
    camera.setExposure(exposure);
    // path = "/media/sd/data/tmp/almog/";
    // path = "/home/nvidia/tmp/";
    path = "/mnt/sd/PODD/tmp/";
    path.append("frame_");
    path.append(std::to_string(camera.GetWidth()));
    path.append("x");
    path.append(std::to_string(camera.GetHeight()));
    path.append("_");
    path.append(camera.GetImageFormat());
    path.append("_");
    path.append(std::to_string(frameIdx));
    path.append("_");
    path.append(std::to_string(exposure));
    path.append(".bin");
    std::cout << path << std::endl;

    { // Validate Exposure
      bool exposureReady = false;
      do {
        exposureReady = camera.GetChangeExposureDone();
        if (!exposureReady)
          std::cout << "MIPI: exposure is not yet Ready" << std::endl;
        usleep(1000);
      } while (!exposureReady);
    }
    if (triggerGPIO) {
      triggerGPIO->setValue(false);
      usleep(1);
      triggerGPIO->setValue(true);

      usleep(1);

      triggerGPIO->setValue(false);
      usleep(1);
      triggerGPIO->setValue(true);

      // usleep(50);

      // triggerGPIO->setValue(false);
      // usleep(50);
      // triggerGPIO->setValue(true);
    }

    camera.DoSnap();
    // usleep(exposure / 1);
    const unsigned char *buffer = camera.GetFrame(&frame_props);
    if (buffer == nullptr) {
      frameIdx--;
      continue;
    }

    // usleep(100000);
    // char *outbuffer = new char[sizeof(char) * camera.GetimageSize()];
    unsigned int imageSize = camera.GetimageSize();
    if (frame_props.frame_idx != frameIdx)
      printf("frameid is incorrect\r\n");
    // memcpy(outbuffer, buffer, imageSize);
    // save_buffer_to_file(buffer, imageSize, path.c_str());

    // unsigned char *dest = new unsigned char[sizeof(char) * camera.getWidth()
    // * camera.getHeight() * 3]; int stride = 1;
    // v4lconvert_uyvy_to_rgb24(buffer, dest, camera.getWidth(),
    // camera.getHeight(), stride, V4L2_PIX_FMT_VYUY); path += ".rgb";
    // save_buffer_to_file(dest, imageSize, path.c_str());
    // delete dest;
    // delete outbuffer;
  }

  clock_gettime(CLOCK_MONOTONIC_RAW, &end);

  double delta_s = 1.0f *
                   ((end.tv_sec - start.tv_sec) * 1000000 +
                    (end.tv_nsec - start.tv_nsec) / 1000) /
                   1000000;
  double fps = 1.0f * n_frames / delta_s;
  cout << "total time:\t" << std::to_string(delta_s) << endl;
  cout << "fps:\t" << std::to_string(fps) << endl;

  camera.StopStream();
  camera.DeInit();

  // Write the data out to file
  // ofstream outFile;
  // outFile.open(outPath, ios::binary);

  // int bufPos = 0, outFileMemBlockSize = 0;        // the position in the
  // buffer and the amoun to copy from
  //                                                 // the buffer
  // int remainingBufferSize = bufferinfo.bytesused; // the remaining buffer
  // size, is decremented by
  //                                                 // memBlockSize amount on
  //                                                 each loop so we do not
  //                                                 overwrite the buffer
  // char *outFileMemBlock = NULL;                   // a pointer to a new
  // memory block int itr = 0;                                    // counts
  // thenumber of iterations

  // outFile.write(buffer, bufferinfo.bytesused);

  // while (remainingBufferSize > 0)
  // {
  //     bufPos += outFileMemBlockSize; // increment the buffer pointer on each
  //     loop
  //                                    // initialise bufPos before
  //                                    outFileMemBlockSize so we can start
  //                                    // at the begining of the buffer

  //     outFileMemBlockSize = 1024; // set the output block size to a
  //     preferable size. 1024 :) outFileMemBlock = new char[sizeof(char) *
  //     outFileMemBlockSize];

  //     // copy 1024 bytes of data starting from buffer+bufPos
  //     memcpy(outFileMemBlock, buffer + bufPos, outFileMemBlockSize);
  //     outFile.write(outFileMemBlock, outFileMemBlockSize);

  //     // calculate the amount of memory left to read
  //     // if the memory block size is greater than the remaining
  //     // amount of data we have to copy
  //     if (outFileMemBlockSize > remainingBufferSize)
  //         outFileMemBlockSize = remainingBufferSize;

  //     // subtract the amount of data we have to copy
  //     // from the remaining buffer size
  //     remainingBufferSize -= outFileMemBlockSize;

  //     // display the remaining buffer size
  //     // cout << itr++ << " Remaining bytes: " << remainingBufferSize <<
  //     endl;

  //     delete outFileMemBlock;
  // }

  // Close the file
  // outFile.close();
}
