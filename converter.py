#!/usr/bin/python3
import sys
import os
import cv2
import numpy as np


def convert(infile, width, height):

    with open(infile, "rb") as f:
        frame = np.fromfile(infile, dtype=np.uint8, count=height*width)
    frame = frame.reshape(height, width)

    frame1 = cv2.cvtColor(frame, cv2.COLOR_BAYER_BG2BGR)

    # cv2.namedWindow("Frame", cv2.WINDOW_KEEPRATIO)
    # cv2.imshow("Frame", frame1)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    outFile = infile + ".png"
    cv2.imwrite(outFile, frame1)


def main():
    if len(sys.argv) < 3:
        print('Not enough arguments, Usage:')
        print(f'python3 {sys.argv[0]} <video_file> <width> <height>')
        print('exiting...')
        sys.exit(1)

    in_dir = sys.argv[1]
    width = int(sys.argv[2])
    height = int(sys.argv[3])

    for file in os.listdir(in_dir):
        filePath = os.path.join(in_dir, file)
        if file.endswith(".bin"):
            print(f'converting {filePath}')
            convert(filePath, width, height)
        else:
            print(f'skipping {filePath}')


if __name__ == "__main__":
    main()
