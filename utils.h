//
// Created by almog on 24/02/21.
//

#ifndef UTILS_H
#define UTILS_H

#include <linux/types.h>
#include <string>

void save_buffer_to_file(const unsigned char *buffer, int size,
                         const char *path);
std::string FourCCToString(__u32 nFourCC);

#endif // UTILS_H