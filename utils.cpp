#include "utils.h"
#include <fstream> //ofstream
#include <string>

using namespace std;

void save_buffer_to_file(const unsigned char *buffer, int size,
                         const char *path) {
  // Write the data out to file
  ofstream outFile;
  outFile.open(path, ios::binary);
  outFile.write((const char *)buffer, size);
  outFile.close();
}

// Helper function to convert fourcc into string
std::string FourCCToString(__u32 nFourCC) {
  const char *pFourCC = (const char *)&nFourCC;
  std::string ans = "";
  for (int i = 0; i < 4; i++)
    ans += pFourCC[i];

  return ans;
}