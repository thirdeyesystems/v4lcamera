//
// Created by almog on 24/02/21.
//

#ifndef VIDEO4LINUXCAMERA_H
#define VIDEO4LINUXCAMERA_H

#include <atomic>
#include <linux/videodev2.h>
#include <mutex>
#include <string>

struct FrameProps {
  uint64_t timestamp_us;
  uint64_t frame_idx;
};

class V4L_Camera {
public:
  enum TRIGGER_MODE { SOFTWARE = 0x00, HARDWARE = 0x01, MASTER = 0xFF };

  V4L_Camera(const V4L_Camera &);
  V4L_Camera();
  virtual ~V4L_Camera();
  int Init(const std::string &path, unsigned int width, unsigned int height,
           unsigned int mOffsetX, unsigned int mOffsetY,
           unsigned int pixelFormat, TRIGGER_MODE trigger_mode);
  int DeInit();
  const unsigned char *GetFrame(FrameProps *frame_props);
  std::string GetImageFormat() const;
  unsigned int GetimageSize() const;
  const unsigned int GetHeight() const { return imageFormat.fmt.pix.height; }
  const unsigned int GetWidth() const { return imageFormat.fmt.pix.width; }
  std::string GetName() const { return cameraName; }
  int StartStream();
  int StopStream();
  const std::string getPath() const { return path; }
  bool DoSnap();
  int setExposure(uint32_t exposure, bool thread = true);
  TRIGGER_MODE getTriggerMode() const;
  bool GetChangeExposureDone();
  int GetAutoGain() const;
  int SetGain(uint32_t gain, bool thread = true);
  int SetAutoGain(bool setAuto);
  int GetManualGain() const;

private:
  static const int nBuffers = 1;
  int fd = -1;
  std::string path = "";
  v4l2_format imageFormat;
  v4l2_crop imageCrop;
  v4l2_buffer mBufferInfo[nBuffers];
  std::string cameraName;
  int mBufIdx = 0;
  int streaming = -1;
  unsigned char *buffer[nBuffers];
  TRIGGER_MODE mTriggerMode;
  int mGainType = -1; // 0 for manual; 1 for auto
  int mGain = -1;
  int mExposureType = -1; // enum v4l2_exposure_auto_type
  int mExposureTime = -1; // 0 for auto exposure
  std::mutex mIOCTL_mtx;
  std::atomic<bool> mChangeExposureDone;
  std::atomic<bool> mChangeGainDone;

  int Dequeue_image(v4l2_buffer &bufferinfo);
  int Queue_image(v4l2_buffer &bufferinfo);
  int setblocking(bool blocking);
  void getExposureBounds(int &eMin, int &eMax);
  int GetManualExposure() const;
  int SetManualExposure(int val);
  int SetManualGain(int val);
  int GetAutoExposure() const;
  int SetAutoExposure(v4l2_exposure_auto_type type);
  void WaitForThreads() const;
};

#endif // VIDEO4LINUXCAMERA_H
